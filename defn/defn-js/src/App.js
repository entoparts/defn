import './App.css';

import DefnService from './api/defnservice';
import { React, useState, useEffect } from 'react';
import { Card, List, Form, Input, Button, Select } from 'antd';

function HistoricalDefn(props) {
  return (
    <List.Item.Meta
      title={<div>{props.word} ({props.source})</div>}
      description={props.defns[0] || props.err_msg}
    />
  )
}

function DefineForm(props) {
  // eslint-disable-next-line no-unused-vars
  const [chosenSource, setChosenSource] = useState(null);
  const [word, setWord] = useState("");
  const { Option } = Select;

  const layout = {
    labelCol: { span: 2 },
    wrapperCol: { span: 6 },
  };
  const tailLayout = {
    wrapperCol: { offset: 2, span: 6 },
  };
  

  const onSourceChange = (value) => {
    setChosenSource(value);
  };

  const onWordChange = (e) => {
    setWord(e.target.value);
  };

  const onFinish = (values) => {
    props.addDefn(values);
    form.resetFields();
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };


  const [form] = Form.useForm();
  return (
    <Form {...layout} form={form} name="control-hooks"
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}>
      <Form.Item name="word" label="word" rules={[{ required: true }]}>
        <Input value={word} onChange={onWordChange}/>
      </Form.Item>
      <Form.Item name="source" label="source" rules={[{ required: true }]}>
        <Select
          placeholder="Select a source dictionary"
          onChange={onSourceChange}
          allowClear
        >
          <Option value="merriam">Merriam Webster</Option>
          <Option value="oxford">Oxford</Option>
        </Select>
      </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Define
        </Button>
      </Form.Item>
    </Form>

  )
}

function App() {

  const [history, setHistory] = useState([]);

  const onSubmitDefn = data => {
    const source = data['source'];
    const word = data['word'];
    DefnService.get(source, word)
    .then(function (response) {
      setHistory(history => [response, ...history]);
    })
    .catch(function (error) {
      console.log(error);
    }) 
  };

  useEffect(() => {
    DefnService.list()
    .then(function (response) {
      setHistory(response);
    })
    .catch(function (error) {
      console.log(error);
    })
  }, []); 

  return(
    <div className="App">
      <h1>&nbsp;&nbsp;&nbsp;Def&apos;n</h1>

      <DefineForm addDefn={onSubmitDefn} />

      <Card className="RecentQueries" title="Recent Queries">
        { 
          history.length > 0 
          && 
          <List
            bordered
            dataSource={history}
            renderItem={defn => (
              <List.Item >
                <HistoricalDefn 
                  err_msg={defn.err_msg}
                  source={defn.source} 
                  word={defn.word} 
                  defns={defn.defns} />
              </List.Item>
            )} />
        }    
      </Card>
    </div>
  )
}

export default App;
