import request from '../utils/requests';

function get(source, word) {
  return request({
    url:    `/defns/${source}/${word}`,
    method: 'GET'
  });
}

function list() {
  return request({
    url:    `/recentdefns`,
    method: 'GET'
  });
}
  
const DefnService = {
  get, list
}
  
export default DefnService;
  