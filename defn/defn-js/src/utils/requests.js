import axios from 'axios';


function getCookie(name) {
  let cookieValue = null;
  if (document.cookie && document.cookie !== '') {
      const cookies = document.cookie.split(';');
      for (let i = 0; i < cookies.length; i++) {
          const cookie = cookies[i].trim();
          if (cookie.substring(0, name.length + 1) === (name + '=')) {
              cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
              break;
          }
      }
  }
  return cookieValue;
}

const csrftoken = getCookie('csrftoken');

const apiClient = axios.create({
  baseURL: '', // need to alter for prod
  headers: {
    common: {
      'X-CSRFToken': csrftoken
    }
  }
})


// https://gist.github.com/sheharyarn/7f43ef98c5363a34652e60259370d2cb
const request = function(options) {
    const onSuccess = function(response) {
      console.debug('Request Successful!', response);
      return response.data;
    }
  
    const onError = function(error) {
      console.error('Request Failed:', error.config);
  
      if (error.response) {
        // Request was made but server responded with something
        // other than 2xx
        console.error('Status:',  error.response.status);
        console.error('Data:',    error.response.data);
        console.error('Headers:', error.response.headers);
  
      } else {
        // Something else happened while setting up the request
        // triggered the error
        console.error('Error Message:', error.message);
      }
  
      return Promise.reject(error.response || error.message);
    }
  
    return apiClient(options)
              .then(onSuccess)
              .catch(onError);
}
  
export default request;
  