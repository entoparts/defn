from django.contrib import admin
from django.urls import include, path, re_path
from mainapp import views
from django.conf import settings

from cra_helper.views import proxy_cra_requests

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('defns/<str:source>/<str:word>', views.DefinitionDetail.as_view()),
    path('recentdefns', views.DefinitionList.as_view()),
]

if settings.DEBUG:
    proxy_urls = [
        re_path(r'^__webpack_dev_server__/(?P<path>.*)$', proxy_cra_requests),
        re_path(r'^(?P<path>.+\.hot-update\.(js|json|js\.map))$', proxy_cra_requests),
    ]
    urlpatterns.extend(proxy_urls)
