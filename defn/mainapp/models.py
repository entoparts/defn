from django.db import models
from django.contrib.postgres.fields import ArrayField


# Create your models here.
class Definition(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    source = models.CharField(max_length=100, blank=True, default='')
    word = models.CharField(max_length=100, blank=True, default='')
    defns = ArrayField(models.CharField(max_length=150, blank=True),)
    err_msg = models.CharField(max_length=100, blank=True, default=None, null=True)
    raw_response = models.JSONField()

    class Meta:
        unique_together = (('source', 'word'),)
