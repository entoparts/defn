from rest_framework import serializers
from .models import Definition

class DefinitionSerializer(serializers.ModelSerializer):
    defns = serializers.ListField(child=serializers.CharField())

    class Meta:
        model = Definition
        fields = ['source', 'word', 'defns', 'created', 'err_msg']
