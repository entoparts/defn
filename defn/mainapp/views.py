from django.shortcuts import render
from rest_framework import generics
from rest_framework.views import APIView

from mainapp.serializers import DefinitionSerializer
from mainapp.models import Definition
from wrappers import API_MAP

def index(request):
    context = {
        'component': 'App',
        'props': {
            'env': 'Django',
        },
    }
    return render(request, 'index.html', context)


def ping_api(source, word):
    target_api = API_MAP.get(source)()
    defns, err_msg, response = target_api.query(word)
    return defns, err_msg, response

class DefinitionList(generics.ListAPIView):
    serializer_class = DefinitionSerializer

    def get_queryset(self):
        return Definition.objects.order_by('-created').all()[:10]

class DefinitionDetail(generics.RetrieveAPIView):
    serializer_class = DefinitionSerializer
    multiple_lookup_fields = ('source', 'word')


    def get_object(self):
        queryset = self.get_queryset()
        filter = {}
        for field in self.multiple_lookup_fields:
            filter[field] = self.kwargs[field]

        defns = Definition.objects.filter(**filter).first()
        if defns is  None:
            defn_object = Definition()
            defns, err_msg, response = ping_api(self.kwargs['source'], self.kwargs['word'])
            defn_object.word = self.kwargs['word'] 
            defn_object.source = self.kwargs['source'] 
            defn_object.defns = defns 
            defn_object.raw_response = response.json()
            defn_object.err_msg = err_msg
            defn_object.save()
            return defn_object
        return defns

    def get_queryset(self):
        word = self.kwargs.get('word')
        source = self.kwargs.get('source')
        defns = Definition.objects.filter(word=word).filter(source=source)
 
