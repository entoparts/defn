from .oxford_api import OxfordAPI
from .merriam_api import MerriamAPI


API_MAP = {
    'oxford': OxfordAPI,
    'merriam': MerriamAPI
}

__all__ = [API_MAP]
