import abc

class APIWrapperBase(object):
    __metaclass__ = abc.ABCMeta
    
    @abc.abstractmethod
    def load_credentials(self):
        """Load API Credentials."""
        return

    @abc.abstractmethod
    def generate_url(self):
        """Generate URL based on attributes."""
        return    

    @abc.abstractmethod
    def query_api(self):
        """Query the API for JSON output."""
        return

    @abc.abstractmethod
    def parse_response(self, response):
        """Parse the JSON output for definitions and errors."""
        return

    @abc.abstractmethod
    def query(self, word):
        """Main function of API class meant to orchestrate everything"""
        return