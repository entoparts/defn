from oxford_api import OxfordAPI
from merriam_api import MerriamAPI
import click

source_api_map = {
    'oxford': OxfordAPI,
    'merriam': MerriamAPI
}

@click.command()
@click.option('--source', default='oxford', help='Source dictionary.')
@click.option('--count', default=1, help='Number of definitions.')
@click.option('--word', prompt='word',
              help='Word you want defined.')
def define(word, count, source):
    target_api = source_api_map.get(source)
    defns, err_msg, response = target_api().query(word)

    if err_msg is not None:
        click.echo(f"{err_msg}")
    else:
        actual_count = min(count, len(defns))
        for idx in range(actual_count):
            click.echo(f"defn: {defns[idx]}")


if __name__ == '__main__':
    define()
