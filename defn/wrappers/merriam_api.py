import requests
import json
import os
import logging

from .apiwrapperbase import APIWrapperBase

logging.basicConfig(level=logging.DEBUG)

class MerriamAPI(APIWrapperBase):

    api_stem = "https://www.dictionaryapi.com/api/v3/references/collegiate/json/"
    # voluminous?key=your-api-key

    def is_list_of_words(self, resp_list):
        return all([isinstance(word, str) for word in resp_list])

    def load_credentials(self):
        try:
            self.api_key = os.getenv('MERRIAM_API_KEY').strip()
        except Exception as e:
            err_msg = str(e)
            logging.error(err_msg)
            raise e

        if self.api_key is None:
            raise("Credentials are missing.")

    def generate_url(self, word):
        self.url = self.api_stem + word.lower() + '?key=' + self.api_key

    # slated for refactoring
    def query_api(self, url):
        err_msg = None
        r = None
        try:
            r = requests.get(self.url)
        except requests.exceptions.ConnectionError:
            err_msg = "Unexpected Connection Error"
        except Exception as e:
            err_msg = str(e)
            logging.error(err_msg)
        return r, err_msg

    def parse_response(self, response):
        defns = []
        err_msg = None
        try:
            word_json = response.json()
            # print(word_json)
            if self.is_list_of_words(word_json):
                err_msg =  "Word is undefined"
            else:
                for entry in word_json:
                    for defn in entry['shortdef']:
                        defns.append(defn)
        except requests.exceptions.ConnectionError:
            err_msg = "Unexpected Connection Error"
        except Exception as e:
            err_msg = str(e)
            logging.error(err_msg)
        return defns, err_msg

    def query(self, word):
        logging.info("Loading credentials")
        self.load_credentials()
        logging.info("Generating URL")
        self.generate_url(word)
        logging.info("Querying API")
        response, err_msg = self.query_api(self.url)
        if err_msg:
            return defns, err_msg
        logging.info("Parse API")
        defns, err_msg = self.parse_response(response)
        return defns, err_msg, response


if __name__ == "__main__":
    mm = MerriamAPI()
    defns, err_msg, response = mm.query('future')
    print(response)
    print(defns)
    print(err_msg)