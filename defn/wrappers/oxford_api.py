import requests
import json
import os
import logging

from .apiwrapperbase import APIWrapperBase

# logging.basicConfig(level=logging.DEBUG)


class OxfordAPI(APIWrapperBase):

    # api specific prereqs
    api_stem = 'https://od-api.oxforddictionaries.com/api/v2/entries/'
    language = 'en-us'
    fields = 'definitions'
    strictMatch = 'true'

    def load_credentials(self):
        try:
            self.app_id = os.getenv('OXFORD_APP_ID').strip()
            self.app_key = os.getenv('OXFORD_APP_KEY').strip()
        except Exception as e:
            err_msg = str(e)
            logging.error(err_msg)
            raise e

        if self.app_id is None or self.app_key is None:
            raise("Credentials are missing.")

    def generate_url(self, word):
        self.url = self.api_stem + self.language + '/' + word.lower() + '?fields=' + self.fields + '&strictMatch=' + self.strictMatch

    # slated for refactoring
    def query_api(self, url):
        err_msg = None
        r = None
        try:
            r = requests.get(self.url, headers = {'app_id': self.app_id, 'app_key': self.app_key})
        except requests.exceptions.ConnectionError:
            err_msg = "Unexpected Connection Error"
        except Exception as e:
            err_msg = str(e)
            logging.error(err_msg)
        return r, err_msg

    def parse_response(self, response):
        defns = []
        err_msg = None
        try:
            word_json = response.json()
            if word_json.get('error'):
                err_msg =  word_json.get('error')
            else:
                for result in word_json.get('results'):
                    lexicalentries = result.get('lexicalEntries')
                    for lexicalentry in lexicalentries:
                        entries = lexicalentry.get('entries')
                        for entry in entries:
                            temp_defns = [sense.get('definitions')[0] for sense in entry.get('senses')]
                            defns.extend(temp_defns)
        except requests.exceptions.ConnectionError:
            err_msg = "Unexpected Connection Error"
        except Exception as e:
            err_msg = str(e)
            logging.error(err_msg)
        return defns, err_msg

    def query(self, word):
        defns = []
        logging.info("Loading credentials")
        self.load_credentials()
        logging.info("Generating URL")
        self.generate_url(word)
        logging.info("Querying API")
        response, err_msg = self.query_api(self.url)
        if err_msg:
            return defns, err_msg, response
        logging.info("Parse API")
        defns, err_msg = self.parse_response(response)
        return defns, err_msg, response

if __name__ == "__main__":
    oo = OxfordAPI()
    defns, err_msg, response = oo.query('future')
    print(response)
    print(defns)
    print(err_msg)