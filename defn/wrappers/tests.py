import vcr
import requests
import unittest

from oxford_api import OxfordAPI
from merriam_api import MerriamAPI


class OxfordTest(unittest.TestCase):

    @vcr.use_cassette('fixtures/oxford_get.yaml')
    def test_oxford_future(self):
        oxford_api = OxfordAPI()
        defns, err_msg, response = oxford_api.query('future')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(defns, ['the time or a period of time following the moment of speaking or writing; time regarded as still to come', 'at a later time; going or likely to happen or exist'])
        self.assertEqual(err_msg, None)

    @vcr.use_cassette('fixtures/oxford_get_fasuture.yaml')
    def test_oxford_fasuture(self):
        oxford_api = OxfordAPI()
        defns, err_msg, response = oxford_api.query('fasuture')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(defns, [])
        self.assertEqual(err_msg, 'No entry found matching supplied source_lang, word and provided filters')


class MerriamTest(unittest.TestCase):

    @vcr.use_cassette('fixtures/merriam_get.yaml')
    def test_merriam_future(self):
        merriam_api = MerriamAPI()
        defns, err_msg, response = merriam_api.query('future')
        expected_defns = ['that is to be; specifically : existing after death', 'of, relating to, or constituting a verb tense expressive of time yet to come', 'existing or occurring at a later time', 'time that is to come', 'what is going to happen', 'an expectation of advancement or progressive development', 'of, relating to, or constituting a verb tense that is traditionally formed in English with will have and shall have and that expresses completion of an action by a specified time that is yet to come', 'the physical and psychological distress suffered by one who is unable to cope with the rapidity of social and technological changes', 'a verb tense that is used to refer to the future', 'an idea, product, or movement that is viewed as representing forces or a trend that will inevitably prevail', 'for the purpose of being looked at for information in the future', 'at a time that is not long from now : soon', 'from now on : from this time onward', 'at a time that is not long from now : soon']
        self.assertEqual(response.status_code, 200)
        self.assertEqual(defns, expected_defns)
        self.assertEqual(err_msg, None)

    @vcr.use_cassette('fixtures/merriam_get_fasuture.yaml')
    def test_merriam_fasuture(self):
        merriam_api = MerriamAPI()
        defns, err_msg, response = merriam_api.query('fasuture')
        expected_list = ['facture', 'future', 'factures', 'suture', 'faster', 'capture', 'fixture', 'couture', 'fixtures', 'feature', 'filature', 'fracture', 'futures', 'gesture', 'mature', 'sutured', 'sutures', 'infrastructure', 'calenture', 'coculture']
        self.assertEqual(response.json(), expected_list)
        self.assertEqual(defns, [])
        self.assertEqual(err_msg, 'Word is undefined')